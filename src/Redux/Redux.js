import { createStore } from "redux";

if (!localStorage.getItem('todoData')) {
    localStorage.setItem('todoData',JSON.stringify({
        todos: [],
        count: 1,
        search: ''
      })
      )
} 

export const addActionCreator = (text)=>{
    return {
      type: 'ADD',
      task: text
    }
  }
  
export const deleteActionCreator = (id)=>{
    return {
      type: 'DELETE',
      identity: id
    }
  }
  
export  const markActionCreator = (id,status)=>{
    return {
      type: 'MARK',
      identity: id,
      active: status
    }
  }
  
export  const searchActionCreator = (value)=>{
    return {
      type: 'SEARCH',
      query: value
    }
  }
let INITIAL_STATE = JSON.parse(localStorage.getItem('todoData'))

  
  const todoReducer = (state= INITIAL_STATE,action)=>{
      let stringObject = JSON.stringify(state)
      let newObject = JSON.parse(stringObject)
    switch (action.type) {
        
      case 'ADD':
        let newToDo = {
          id:newObject['count'],
          completed: false,
          task:action.task
        }
        newObject['count'] +=1
        newObject['todos'] = [newToDo,...newObject['todos']]
        console.log(newObject)
        return newObject
      case 'DELETE':
        newObject['todos'] =  newObject.todos.filter((elem)=> elem['id'] !== action.identity)
        console.log('deleted')
        return newObject
      case 'MARK':
        newObject['todos'].forEach((elem) => {
          if (Number(elem['id']) === Number(action.identity)) {
            console.log('happening')
            elem.completed = action.active
          } 
        });
        console.log('mark',newObject)
        return newObject
      case 'SEARCH':
        newObject.search = action.query
        return newObject
      default:
        return state
  
  
    }
  }
  
 const store = createStore(todoReducer)

 store.subscribe(()=>{
    localStorage.setItem('todoData',JSON.stringify(store.getState()))
 })

 export default store