import React from "react";
import List from "./List";
import './Display.css'
class Display extends React.Component {
    constructor(props){
      super()
    }
    inputHandler = (event)=>{
      if ( event.target.value !== '' && event.key === 'Enter') {
        
          this.props.addNewTodo(event.target.value)
          event.target.value = ''
        
      }
    }
    deleteHandler = (event) => {
      this.props.deleteTodo(Number(event.target.id))
    }
    changeHandler = (event) => {
      this.props.markTodo(event.target.id,event.target.checked)
    }
    searchHandler = (event) => {
        console.log(event.target.value)
        this.props.searchTodo(event.target.value)
      
     
    }
     
    render () {
        let  searchQuery = this.props.todoState.search
        let  todoArray = this.props.todoState.todos
        let  filteredArray = todoArray.filter((item)=>item.task.toLowerCase().includes(searchQuery.toLowerCase()))
      if (filteredArray.length === 0) {
        filteredArray = todoArray.slice()
      }
      
      return (
        <div className="todo-container">
            <h1>Todos</h1>
        <div className="contents">
          <div className="search">
            <input type='text' placeholder="search what you want" onChange={this.searchHandler} className='input-bar'></input>
          </div>
          <ul>
            {!searchQuery ? 
            <List 
            Array={todoArray} 
            deleteHandler={this.deleteHandler}
            changeHandler={this.changeHandler}
            >   
            </List>: 
            <List 
            Array={filteredArray}
            deleteHandler={this.deleteHandler}
            changeHandler={this.changeHandler}></List>}
          </ul>
          <div className="input">
            <input type='text' placeholder="what to do next" onKeyUp={this.inputHandler} className='input-bar'></input>
          </div>
        </div>
        </div>
      )
    }
  }

  export default Display