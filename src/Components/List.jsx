import React from "react";
import './List.css'

class List extends React.Component {
    constructor (props){
        super()
    }
    render () {
        return (this.props.Array.map((item)=>{
            return (<li key={item.id+'yo'}>
              <input type='checkbox' id={item.id} onChange= {this.props.changeHandler} checked={item.completed} className='completed'></input>
              <p className={item.completed? 'todo-para strike-through' : 'todo-para red'}>{item.task}</p>
              <button id={item.id} onClick = {this.props.deleteHandler}>Delete</button>
            </li>)
          }))
    }
}

export default List