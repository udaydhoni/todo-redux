import React from "react";
import { connect, Provider } from "react-redux";
import './App.css'
import { addActionCreator,deleteActionCreator,markActionCreator,searchActionCreator  } from "./Redux/Redux";
import store from "./Redux/Redux";
import Display from "./Components/Display";


const mapStateToProps = (state)=>{
  return {todoState: state}
}

const mapDispatchToProps = (dispatch) =>{
  return {
    addNewTodo: (todo)=>{dispatch(addActionCreator(todo))},
    deleteTodo: (id)=>{dispatch(deleteActionCreator(id))},
    markTodo: (id,status)=>{dispatch(markActionCreator(id,status))},
    searchTodo: (term)=>{dispatch(searchActionCreator(term))}
  }
}

let CONTAINER = connect(mapStateToProps,mapDispatchToProps)(Display)



class App extends React.Component {
  constructor(props) {
    super()
  }
  render () {
    return (
      <Provider store={store}>
        <CONTAINER></CONTAINER>
      </Provider>
    )
  }
}

export default App